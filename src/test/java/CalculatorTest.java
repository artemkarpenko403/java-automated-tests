import com.aqa.course.Calculator;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;


import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * CalculatorTest includes tests for Calculator class.
 *
 * @author Artem
 * @version 1.0
 */
public class CalculatorTest {
    private Calculator calculator;
    private SoftAssertions softly;

    @BeforeEach
    public void setup() {
        this.calculator = new Calculator();
        this.softly = new SoftAssertions();
    }

    @AfterEach
    public void assertAll() {
        this.softly.assertAll();
    }

    @RepeatedTest(value = 2, name = "{displayName} {currentRepetition}/{totalRepetitions}")
    public void repeatedTest() {
    }

    /**
     * Positive test for checking positive summary.
     *
     * @param firstValue
     * @param secondValue
     * @param expectedResult
     */

    @Tag("Positive")
    @DisplayName("Positive sum test")
    @ParameterizedTest
    @CsvSource({
            "1,2,3",
            "-10,-20,-30"
    })
    public void sumTest(int firstValue, int secondValue, int expectedResult) {

        int actualResult = calculator.sum(firstValue, secondValue);

        softly.assertThat(actualResult).isEqualTo(expectedResult);

    }

    /**
     * Negative test to check positive summary.
     *
     * @param firstValue
     * @param secondValue
     * @param expectedResult
     */

    @Tag("Negative")
    @DisplayName("Negative sum test")
    @CsvSource({
            "1,2,5",
            "-10,-20,-10"
    })
    public void sumNegativeTest(int firstValue, int secondValue, int expectedResult) {

        int actualResult = calculator.sum(firstValue, secondValue);

        softly.assertThat(actualResult).isNotEqualTo(expectedResult);

    }

    /**
     * Positive number division test.
     *
     * @param firstValue
     * @param secondValue
     * @param expectedResult
     */

    @DisplayName("Positive number division test")
    @Tag("Positive")  //Negative
    @ParameterizedTest
    @CsvFileSource(resources = "/data-test.csv")
    public void divideTest(int firstValue, int secondValue, int expectedResult) {

        double actualResult = calculator.divide(firstValue, secondValue);

        softly.assertThat(actualResult).isEqualTo(expectedResult);

    }

    /**
     * Negative number division test.
     *
     * @param firstValue
     * @param secondValue
     * @param expectedResult
     */

    @DisplayName("Negative number division test")
    @Tag("Negative")  //Negative
    @ParameterizedTest
    @CsvFileSource(resources = "/data-negativeness.csv")
    public void divideNegativeTest(int firstValue, int secondValue, int expectedResult) {

        double actualResult = calculator.divide(firstValue, secondValue);

        softly.assertThat(actualResult).isNotEqualTo(expectedResult);

    }

    private static Stream<Arguments> testDataForSumOfCollectionValues() {
        return Stream.of(
                Arguments.of(Arrays.asList(1, 3, 4), 8),
                Arguments.of(Arrays.asList(-1, 0, 1), 0)
        );
    }

    /**
     * Positive test for collection amount.
     *
     * @param numbers
     * @param expectedResult
     */
    @Disabled
    @DisplayName("Collection value sum")
    @ParameterizedTest
    @Tag("Positive")
    @MethodSource("testDataForSumOfCollectionValues")
    public void sumOfCollectionValuesTest(List<Integer> numbers, long expectedResult) {
        long actualResult = calculator.sum(numbers);

        softly.assertThat(actualResult).isEqualTo(expectedResult);
    }

    private static Stream<Arguments> testNegativeDataForSumOfCollectionValues() {
        return Stream.of(
                Arguments.of(Arrays.asList(1, 3, 4), 1),
                Arguments.of(Arrays.asList(-1, 0, 1), 2)
        );
    }

    /**
     * Negative test for collection amount.
     *
     * @param numbers
     * @param expectedResult
     */

    @DisplayName("Collection value sum")
    @ParameterizedTest
    @Tag("Negative")
    @MethodSource("testNegativeDataForSumOfCollectionValues")
    public void sumOfCollectionValuesNegativeTest(List<Integer> numbers, long expectedResult) {
        long actualResult = calculator.sum(numbers);

        softly.assertThat(actualResult).isNotEqualTo(expectedResult);
    }
}

